"""
Author: Rodrigo Muñoz L.
"""
from numpy import pi, prod, cos, sin, exp, sum, abs, sqrt, ceil
from scipy.special import erfcinv, erfc, jve
from scipy.optimize import newton


def pe_delta1_err(g0, gk, T):
    sqrt2 = sqrt(2)
    x = T/2 - (abs(g0)+sum(abs(gk)))
    err = 1/(2*sqrt2) * erfc(x/sqrt2)
    return err

def pe_delta2_err (T, M):
    sqrt2 = sqrt(2)
    sqrtpi = sqrt(pi)
    err = (sqrtpi*T)/(2*(pi**2)*M) * erfc((sqrt2*pi*M)/T)
    return err

def pe_delta4_err(g0, Rk, T):
    sqrt2 = sqrt(2)
    x = T/2 - abs(g0) - sum(Rk)
    err = 1/(2*sqrt2) * erfc(x)
    return err

def pe_delta5_err(g0, gk, Rk, T):
    sqrt2 = sqrt(2)
    x = T/2 - abs(g0) - sum(abs(gk)) - sum(Rk)
    err = 1/(2*sqrt2) * erfc(x)
    return err

def pe_T_d1_est(g0, gk, delta1):
    gsum = abs(g0) + sum(abs(gk))
    sqrt2 = sqrt(2)
    T = 2*(sqrt2*erfcinv(2*sqrt2*delta1)+gsum)
    return T

def pe_T_d4_est(g0, Rk, delta4):
    gsum = abs(g0) + sum(Rk)
    sqrt2 = sqrt(2)
    T = 2*(sqrt2*erfcinv(2*sqrt2*delta4) + gsum)
    return T


def pe_T_d5_est(g0, gk, Rk, delta5):
    gsum = abs(g0) + sum(Rk) + sum(abs(gk))
    sqrt2 = sqrt(2)
    T = 2*(sqrt2*erfcinv(2*sqrt2*delta5) + gsum)
    return T

def pe_M_est(delta2, T, M0):
    f = lambda x : pe_delta2_err(T, x) - delta2
    Me = newton(f, M0,maxiter=1000)
    M = int(ceil(Me))
    return M

def pe_isi_awgn(g0, gk, M, Tc):
    """

    Parameters
    ----------
    g0
    gk
    M
    Tpe

    Returns
    -------

    """
    w = 2*pi/Tc
    pe = 0
    for m in range(1,M,2): 
        mult = prod(cos(m*w*gk))
        pe += (exp(-(m*w)**2/2) * sin(m*w*g0))/m * mult
    pe_isi = 0.5 -2/pi * pe
    err_est = pe_delta1_err(g0,gk,Tc)+pe_delta2_err(Tc,M)
    return  pe_isi, err_est

def pe_cci_awgn(g0, Rk, M, Tc):
    w = 2*pi/Tc
    pe = 0 
    for m in range(1,M,2):
        mult = prod(jve(0,m*w*Rk))
        pe += (exp(-(m*w)**2/2) * sin(m*w*g0))/m * mult
    pe_cci = 0.5 - 2/pi*pe
    err_est = pe_delta2_err(Tc, M) + pe_delta4_err(g0, Rk, Tc)
    return pe_cci, err_est

def pe_isi_cci_awgn(g0, gk, Rk, M, Tc):
    w = 2*pi/Tc
    pe = 0 
    for m in range(1,M,2):
        mult_isi = prod(cos(m*w*gk))
        mult_cci = prod(jve(0,m*w*Rk))
        pe += (exp(-(m*w)**2/2) * sin(m*w*g0))/m * mult_cci * mult_isi
    pe_cci = 0.5 -2/pi * pe
    err_est = pe_delta2_err(Tc, M) + pe_delta5_err(g0, gk, Rk, Tc)
    return pe_cci, err_est