"""
Created on Sat Apr 24 00:31:51 2021

@author: Rodrigo Muñoz
"""
from numpy import pi, sinc, sin, cos, log, exp, absolute, empty, size

def RC(t,Ts,alpha):
    """
    Pulso 'Raised Cosine'
    Parameters
    ----------
    t : numpy array
        vector con los tiempos en los cuales se desea evaluar el pulso.
    Ts : float
        Tiempo de simbolo.
    alpha : float
        factor de roll-off.

    Returns
    -------
    numpy array con los valores del pulso.

    """
    rc = empty(size(t))
    tt = t/Ts
    i = absolute(absolute(alpha*tt)-0.5) > 1e-5
    rc[i] = sinc(tt[i])*cos(pi*alpha*tt[i])/(1-(2*alpha*tt[i])**2)
    if not i.all():
        rc[~i] = pi*sinc(tt[~i])*sin(pi*alpha*tt[~i])/(8*alpha*tt[~i])
    
    return rc

def BTRC(t, Ts, alpha):
    """
    Better Than Raised Cosine
    (ec.22 de "Parametric Construction of Nyquist-I Pulses")
    
    Parameters
    ----------
    t : numpy array
        vector con los tiempos en los cuales se desea evaluar el pulso.
    Ts : float
        tiempo de simbolo.
    alpha : float
        factor de roll-off.

    Returns
    -------
    numpy array con los valores del pulso.

    """
    r = empty(size(t))
    tt=t/Ts
    beta = (pi*alpha)/log(2)
    r = sinc(tt)*(2*beta*tt*sin(alpha*pi*tt)+\
                  2*cos(alpha*pi*tt)-1)/((beta*tt)**2+1)
    
    return r

def ELP(t,Ts,beta,alpha):
    """
    Exponential Linear Pulse

    Parameters
    ----------
    t : numpy array
        vector con los tiempos en los cuales se desea evaluar el pulso.
    Ts : float
        tiempo de simbolo.
    beta : float
        DESCRIPTION.
    alpha : float
        factor de roll-off.

    Returns
    -------
    numpy array con los valores del pulso.

    """
    tt = t/Ts
    r = exp(-pi*(beta/2)*((tt)**2)) * sinc(tt)*sinc(alpha*tt)
    
    return r

def IPLCP(t, Ts, mu, epsilon, gamma, alpha):
    """
    Improved Parametric Linear Combination Pulse
    (ec.2 "Evaluation of the Improved Parametric Linear Combination Pulse in 
     Digital Baseband Communication Systems")

    Parameters
    ----------
    t : numpy array
        vector con los tiempos en los cuales se desea evaluar el pulso.
    Ts : float
        tiempo de simbolo.
    mu : float
        DESCRIPTION.
    epsilon : float
        DESCRIPTION.
    gamma : float
        DESCRIPTION.
    alpha : float
        factor de roll-off.

    Returns
    -------
    numpy array con los valores del pulso.

    """
    tt = t/Ts
    r = sinc(tt)*((1-mu)*sinc(alpha*tt/2)**2 + mu*sinc(alpha*tt))
    if gamma != 1:
        r = r**gamma
    rr = exp(-epsilon*(pi*tt)**2)*r
    
    return rr
    