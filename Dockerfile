FROM python:3.9
RUN apt-get update
WORKDIR /proyecto1
COPY requirements.txt ./
VOLUME ["/proyecto1"]
RUN apt-get install -y cm-super \
    dvipng texlive-latex-extra texlive-fonts-recommended
ARG user=appuser
ARG group=appuser
ARG uid=1000
ARG gid=1000
RUN groupadd -g ${gid} ${group}
RUN useradd -u ${uid} -g ${group} -s /bin/sh -m ${user}
USER ${uid}:${gid}
RUN pip install -r requirements.txt
CMD ["python3","main.py"]
