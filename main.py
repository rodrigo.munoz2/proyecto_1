import os
import numpy as np
import pandas as pd
from matplotlib.collections import LineCollection
import matplotlib.pyplot as plt
from pulses import RC, BTRC, ELP, IPLCP
from ber import *

plt.rcParams["figure.dpi"] = 150
plt.rcParams.update({"text.usetex": True})
plt.rcParams.update({'font.size': 14})
plt.close("all")
#creacion de la carpeta para guardar los resultados de la simulacion
dirplot = 'img'
if not os.path.exists(dirplot):
    os.mkdir(dirplot)
######################################################################
# Simulacion Respuesta al Impulso y Respuesta en Frecuencia
######################################################################

# Parametros de simulacion temporal y frecuencial
# -----------------------------------------------
Ts = 1 #tiempo de simbolo
beta_elp = 1 #parametro beta del pulso ELP
mu_iplcp = 1.6 #parametro mu del pulso IPLCP
epsilon_iplcp = 0.1 #parametro epsilon del pulso IPLCP
gamma_iplcp = 1 #paramentro gamma del pulso IPLCP
nfft = 1024 #numero de puntos en el calculo de la FFT
nulos = 3 #numero de nulos a visualizar temporalmente
t_end = Ts*nulos + Ts * 0.6 #tiempo final visualizacion pulsos
alphav = [0.22, 0.55] #factores de alpha
dirtemp = 'resp_temporal' #directorio de graficas de resp. temporal

#creacion del directorio para guardar las figuras
dirtemp_total = os.path.join(dirplot, dirtemp)
if not os.path.exists(dirtemp_total):
    os.mkdir(dirtemp_total)

#diccionario con las funciones temporales de los pulsos
pulsos = {"RC": RC,
          "BTRC": BTRC,
          "IPLCP": lambda ly, lTs, lalpha: IPLCP(ly, lTs, mu_iplcp,
                                                 epsilon_iplcp, 
                                                 gamma_iplcp, 
                                                 lalpha),
          "ELP": lambda ly, lTs, lalpha: ELP(ly, lTs, beta_elp,
                                             lalpha),
          }
mark = {"RC": 'b--', "BTRC": 'r-.', "ELP": 'g:', "IPLCP": 'y-'}
t = np.linspace(0, t_end, 100)  # tiempo a visualizar cada pulso
t_norm = t / Ts #tiempo normalizado

# bucles de simulación de pulsos temporales
# ----------------------------------------
# graficas de cada pulso con todos los factores de alpha dados
for alpha in alphav:
    plt.figure()
    for k in pulsos.keys():
        y = pulsos[k](t, Ts, alpha) #calculo de pulso temporal
        plt.plot(t_norm, y, mark[k], label=k) #grafica del pulso
    plt.grid(True)
    plt.xlabel(r'Tiempo normalizado $t/T_s$')
    plt.title(r'Respuesta Temporal de los Pulsos con $\alpha=$' + \
              str(alpha))
    plt.legend()
    plotfile = 'pulsos_alpha_0' + str(int(alpha * 100)) + '.png'
    plt.savefig(os.path.join(dirtemp_total, plotfile)) #guarda .png

# graficas de todos los pulsos para cada valor de alpha
for k in pulsos.keys():
    plt.figure()
    for alpha in alphav:
        y = pulsos[k](t, Ts, alpha)
        plt.plot(t_norm, y, label=k + r', $\alpha=$' + str(alpha))
    plt.grid(True)
    plt.legend()
    plt.title(r'Respuesta Temporal del Pulso ' + str(k))
    plt.xlabel(r'Tiempo normalizado $t/T_s$')
    plotfile = k + '.png'
    plt.savefig(os.path.join(dirtemp_total, plotfile))#guarda .png

# Prarametros de Simulacion respuesta Frecuencial
# -----------------------------------------------
fn = 1 / Ts #frecuencia de simbolo
fmax = (1+np.max(alphav))/(2*Ts) #aproximacion. frec. max. de pulsos
fm = 4 * fmax  # frecuencia de muestreo
t = np.linspace(-nfft/(2 * fm),nfft/(2*fm),nfft) #tiempo simulacion
f_norm = np.linspace(-fm+fm/(nfft/2),fm,nfft)/fn #frec. normalizada
it_mn = int(nfft/2) #frec. de inicio de las graficas
it_mx = int(nfft) #frec. final de las graficas
dirfrec = 'resp_frecuencial' #directorio de graficas resp. frecuencia

#creacion del directorio para guadar las figuras de respuesta en frec.
dirfrec_total = os.path.join(dirplot, dirfrec)
if not os.path.exists(dirfrec_total):
    os.mkdir(dirfrec_total)

# Bucles de simulacion de respuesta en frecuencia de los pulsos
#--------------------------------------------------------------
# graficas de cada pulso con todos los valores de alpha dados
for alpha in alphav:
    plt.figure()
    for k in pulsos.keys():
        y = pulsos[k](t, Ts, alpha) #pulso temporal
        f = np.abs(np.fft.fftshift(np.fft.fft(y)))/fm #resp. frec.
        plt.plot(f_norm[it_mn:it_mx], f[it_mn:it_mx], mark[k], \
                 label=k)
    plt.grid(True)
    plt.legend()
    plt.xlabel(r'Frecuencia Normalizada')
    plt.ylabel(r'Magnitud')
    plt.title(r'Respuesta Frecuencial de los Pulsos con $\alpha=$' + \
              str(alpha))
    plotfile = 'pulsos_alpha_0'+str(int(alpha*100))+'.png'
    plt.savefig(os.path.join(dirfrec_total, plotfile))

# graficas de todos los pulsos para cada valor de alpha
for k in pulsos.keys():
    plt.figure()
    for alpha in alphav:
        y = pulsos[k](t, Ts, alpha)#pulso temporal
        f = np.abs(np.fft.fftshift(np.fft.fft(y)))/fm #resp. frec.
        plt.plot(f_norm[it_mn:it_mx], f[it_mn:it_mx], \
                 label=k + r', $\alpha=$' + str(alpha))
    plt.grid(True)
    plt.legend()
    plt.ylabel('Magnitud')
    plt.xlabel(r'Frecuencia Normalizada')
    plt.title(r'Respuesta en Frecuencia del Pulso ' + k)
    plotfile = k + '.png'
    plt.savefig(os.path.join(dirfrec_total, plotfile))

plt.close('all')
######################################################################
# Simulacion del Diagrama de Ojo
######################################################################

# Parametros de simulacion
# -------------------------
nsymbols = int(1e3+2) #numero de simbolos (en este caso bits, BPSK)
alphav = [0.22, 0.55] #factor de alpha para el patron de ojo
pot_ruido = [15, 30] #SNR en dB
nulos = 200 #nulos por lado de cada simulacion
#mensaje bits BPSK (-1,1) con pdf uniforme
bpsk_bits = np.random.randint(0,2,nsymbols)*2-1
msymbol = 40 #muestras por simbolo (siempre par)
lcolors_eye = ['blue','red'] #colores de diagrama de ojo para cada SNR

#creacion de carpeta para guardar los diagramas de ojo
direye = 'diagrama_ojo'
direye_total = os.path.join(dirplot, direye)
if not os.path.exists(direye_total):
    os.mkdir(direye_total)

fm = msymbol/Ts #frecuencia de muestreo
Np = int(2*Ts*nulos*fm+1) #muestras para obtener los nulos deseados
t = np.linspace(-Ts*nulos,Ts*nulos,Np) #vector de tiempo de muestras
Nm = Np+(nsymbols-1)*msymbol #numero muestras por mensaje
offset = int((nulos)*msymbol)
max_isi = [] #lista de diccionarios para distorsion max. debido a ISI
#psenal=[]
#Bucle de simulacion de diagrama de ojo
#--------------------------------------
for alpha in alphav:
    max_isi.append({'alpha': alpha})
    for k in pulsos.keys(): #para cada pulso
        y = pulsos[k](t,Ts,alpha) #calculo temporal del pulso
        yt = np.zeros(Nm) #vector para guardar el mensaje total
        i = 0
        #calculo del mensaje temporal total con el pulso tipo k-esimo
        for b in bpsk_bits: #para cada bit del mensaje
            ini = i*msymbol
            yt[ini:(ini+Np)] += b*y #pulso x bit y suma c/anteriores
            i += 1
        max_isi[-1][k] = np.max(np.abs(yt[0::int(msymbol/2)]))#max.ISI
        psenal = np.sum(yt**2)/Nm #potencial de la senal
        ic = 0
        fig = plt.figure()
        for pruido in pot_ruido: #para cada valore de SNR
            print("Diagrama ojo de " + k + " con alpha de " + 
                  str(alpha) + " SNR de " + str(pruido) + "dB")
            pruidol = np.sqrt(psenal*(10**(-pruido/10))) #desv. estandar
            ruido = np.random.normal(0,pruidol,Nm) #ruido temporal
            ytr = yt+ruido #mensaje mas ruido
            lc = np.zeros((nsymbols-2,2*msymbol,2)) #matriz de pulsos
            t_lc = t[(offset-msymbol):(offset+msymbol)]
            for i in range(0,nsymbols-2): #para cada pulso en mensaje
                #obtener cada pulso en el mensaje entre +- t/T
                sym_ini = (offset+i*msymbol) #t=-1 del pulso i-esimo
                sym_end = (offset+(i+2)*msymbol) #t=1 del pulso i-esimo
                lc[i, :, 1] = ytr[sym_ini:sym_end] #pulso i-esimo
                lc[i, :, 0] = t_lc #eje temporal del pulso 
            ax = fig.add_subplot() #subgrafica diag. ojo para cada SNR
            ax.set_ylim(-2.2,2.2)
            lincoll = LineCollection(lc, linestyles='-', 
                                     color=lcolors_eye[ic],
                                     label='AWGN='+str(pruido)+'dB')
            ax.add_collection(lincoll)#metodo eficiente para graficas
            ic += 1
        ax.set_xlim(-1,1)
        ax.set_xlabel(r'Tiempo Normalizado $t/T_s$')
        ax.set_title("Diagrama de Ojo "+k+r', $\alpha=$'+str(alpha))
        plt.grid(True)
        plt.legend()
        plotfile = k + '_a' + str(int(alpha * 100)) + '_n.png'
        fig.savefig(os.path.join(direye_total, plotfile))#guardar .png
        plt.close(fig)
# distorsion maxima ISI
# ---------------------
max_isi_df = pd.DataFrame(max_isi)
print("\nDistorsion maxima debido a ISI:")
print("===============================")
print(max_isi_df)
isifile = 'isi_distortion.csv'
isifile_path = os.path.join(dirplot, isifile)
max_isi_df.to_csv(isifile_path,index=False,float_format='%.4f')

######################################################################
# Calculo del BER
######################################################################
nsymbols = int(2**10) #numero de simbolos en mensaje para simulacion
snrv = np.array([10,15]) #vector con SNR a simular
alphav = np.array([0.22,0.35,0.50]) #vector de alpha a simular
jitterv = np.array([0.05,0.1,0.2,0.25])*Ts #jitter's a simular
sirv = np.array([10,20]) #vector de SIR a sumular  
#mensaje completo BPSK (-1,1) con pdf uniforme
bpsk_bits = np.random.randint(0,2,nsymbols)*2-1
tmn = np.linspace(-int(nsymbols/2)*Ts,-Ts,int(nsymbols/2))
tmx = np.linspace(Ts,int(nsymbols/2)*Ts,int(nsymbols/2))
t = np.concatenate((tmn,tmx))
pe_isi = [] #lista para resultados de la Prob. error debido a ISI 

# Calculo del error solo con ISI
#-------------------------------
#estimacion T y M segun cota de error de estimacion < delta1+delta2
delta1_esp = 5e-13
delta2_esp = 5e-13
delta_tot = delta1_esp+delta2_esp #cota para error de estimacion de Pe

for snr in snrv:
    s = 10**(snr/20)
    for alpha in alphav:
        for jttr in jitterv:
            pe_isi.append({'SNR':snr,'rolloff':alpha,'jitter':jttr})
            for k in pulsos.keys(): #para cada tipo de pulso
                g0 = s*pulsos[k](np.array([jttr]),Ts,alpha)[0]
                gk = s*bpsk_bits*pulsos[k](jttr-t,Ts,alpha)
                #calculo de T y M para cumplir con error de estimacion
                Te_isi = pe_T_d1_est(g0,gk,delta1_esp)
                M = pe_M_est(delta2_esp, Te_isi, 10)+1
                pe,err_est = pe_isi_awgn(g0, gk, M, Te_isi)#calculo Pe
                if (err_est > delta_tot):
                    raise Exception("error de estimación Pe ISI>cota")
                pe_isi[-1][k] = pe #guarda el error calculado
#creacion de archivo csv con los errores debido a ISI
pe_isifile = 'pe_isi.csv'
pe_isifile_path = os.path.join(dirplot, pe_isifile)
pe_isi_df = pd.DataFrame(pe_isi)
print("\nError debido a ISI")
print("==================")
print(pe_isi_df)
pe_isi_df.index += 1
pe_isi_df.to_csv(pe_isifile_path,index=True,index_label='Fila',
                 float_format='%.3e')

# Calculo del error solo con CCI
#-------------------------------
L = [2, 6] #vector con el numero de interferencias co-canal a simular
delta1_esp = 5e-13
delta4_esp = 5e-13
delta_tot = delta1_esp+delta4_esp
pe_cci = [] #lista para resultados de la Prob. de error debido a CCI

for l in L:
    for sir in sirv:
        for snr in snrv:
            s = 10**(snr/20)
            Rk = s/np.sqrt(l*10**(sir/10))*np.ones(l)
            for alpha in alphav:
                for jttr in jitterv:
                    pe_cci.append({'SNR':snr,'rolloff':alpha,
                                   'SIR':sir,'L': l,'jitter':jttr})
                    for k in pulsos.keys():
                        #valor medio del pulso en la mitad del mensaje
                        g0 = s*pulsos[k](np.array([jttr]),Ts,alpha)[0]
                        #magnitud de la interferencia co-canal
                        Te_cci = pe_T_d4_est(g0, Rk, delta4_esp)
                        M = pe_M_est(delta1_esp, Te_cci, 10)
                        #estimacion de Prob. error debido a CCI
                        pe, err_est = pe_cci_awgn(g0, Rk, M, Te_cci)
                        if (err_est > delta_tot):
                            raise Exception("error est. Pe CCI>cota")
                        pe_cci[-1][k] = pe #guadar el error calculado
#creacion de archivo csv con los errores debido a CCI
pe_cci_df = pd.DataFrame(pe_cci)
print("\nError debido a CCI")
print("==================")
print(pe_cci_df)
ih = pe_cci_df.columns.get_loc('jitter')
for jttr in jitterv:
    pe_ccifile = 'pe_cci_'+'jitter_0'+'{:03d}'.format(int(jttr*100))+\
                '.csv'
    pe_ccifile_path = os.path.join(dirplot, pe_ccifile)
    pe_cci_df.loc[pe_cci_df['jitter']==jttr].to_csv(pe_ccifile_path,
                                columns=pe_cci_df.columns.delete(ih),
                                index=False,float_format='%.3e')

# Calculo del error con ISI y CCI
#--------------------------------
snrv = [15,]
L = [2,6]
sirv = [15,]
delta5_esp = 1e-12
delta_tot = delta1_esp + delta5_esp
pe_isi_cci = []

for l in L:
    for sir in sirv:
        for snr in snrv:
            s = 10**(snr/20)
            for alpha in alphav:
                for jttr in jitterv:
                    pe_isi_cci.append({'SNR':snr,'rolloff':alpha,
                                       'SIR':sir,'L': l,
                                       'jitter':jttr})
                    for k in pulsos.keys():
                        #valor medio del pulso en la mitad del mensaje
                        g0 = s*pulsos[k](np.array([jttr]),Ts,alpha)[0]
                        Rk = g0/np.sqrt(l * 10**(sir/10.0)) * np.ones(l)
                        #valor medio de los demas pulsos en el mensaje
                        gk = s*bpsk_bits*pulsos[k](t + jttr,Ts,alpha)
                        #magnitud de la interferencia co-canal
                        Te = pe_T_d5_est(g0, gk, Rk, delta5_esp)
                        M = pe_M_est(delta1_esp, Te, 10)
                        #estimacion de Prob. error debido a CCI+ISI
                        pe, err_est = pe_isi_cci_awgn(g0,gk,Rk,M,Te)
                        if (err_est > delta_tot):
                            raise Exception("error est. Pe ISI+CCI>cota")
                        pe_isi_cci[-1][k] = pe #guardar error estimado
#creacion de archivo csv con los errores debido a CCI
pe_isiccifile = 'pe_isi_cci.csv'
pe_isiccifile_path = os.path.join(dirplot, pe_isiccifile)

pe_isicci_df = pd.DataFrame(pe_isi_cci)
print("\nError debido a ISI y CCI")
print("========================")
print(pe_isicci_df)
pe_isicci_df.to_csv(pe_isiccifile_path,index=False,float_format='%.3e')

######################################################################
# Calculo del BER pulso truncado
######################################################################
nsymbols = int(2**10) #numero de simbolos en mensaje para simulacion
snrv = np.array([10,15]) #vector con SNR a simular
alphav = np.array([0.22,0.35,0.50]) #vector de alpha a simular
jitterv = np.array([0.05,0.1,0.2,0.25])*Ts #jitter's a simular
sirv = np.array([10,20]) #vector de SIR a sumular
nulosv = np.array([3,5]) #nulos a considrar en el truncamiento  
pe_isi_truc = [] #lista para resultados de la Prob. error debido a ISI

# Calculo del error solo con ISI, pulso trucado
#----------------------------------------------
#estimacion T y M segun cota de error de estimacion < delta1+delta2
delta1_esp = 5e-13
delta2_esp = 5e-13
delta_tot = delta1_esp+delta2_esp #cota para error de estimacion de Pe
for nulos in nulosv:
    tmn = np.linspace(-nulos*Ts, -Ts, nulos)
    tmx = np.linspace(Ts, nulos*Ts, nulos)
    t = np.concatenate((tmn,tmx))
    #selccion de bits que inciden en g0 despues del truncamiento
    bpsk_bits_t = bpsk_bits[int(nsymbols/2)-nulos-1:nulos+int(nsymbols/2)-1]
    for snr in snrv:
        s = 10**(snr/20)
        for alpha in alphav:
            for jttr in jitterv:
                pe_isi_truc.append({'nulos':nulos,'SNR':snr,'rolloff':alpha,
                                    'jitter':jttr})
                for k in pulsos.keys(): #para cada tipo de pulso
                    g0 = s*pulsos[k](np.array([jttr]),Ts,alpha)[0]
                    gk = s*bpsk_bits_t*pulsos[k](jttr-t,Ts,alpha)
                    #calculo de T y M para cumplir con error de estimacion
                    Te_isi = pe_T_d1_est(g0,gk,delta1_esp)
                    M = pe_M_est(delta2_esp, Te_isi, 10)+1
                    pe,err_est = pe_isi_awgn(g0, gk, M, Te_isi)#calculo Pe
                    if (err_est > delta_tot):
                        raise Exception("error de estimación Pe ISI>cota")
                    pe_isi_truc[-1][k] = pe #guarda el error calculado
#creacion de archivo csv con los errores debido a ISI
pe_isifile = 'pe_isi_truc.csv'
pe_isifile_path = os.path.join(dirplot, pe_isifile)
pe_isi_df_truc = pd.DataFrame(pe_isi_truc)
print("\nError debido a ISI pulso truncado")
print("===================================")
print(pe_isi_df)
pe_isi_df_truc.index += 1
pe_isi_df_truc.to_csv(pe_isifile_path,index=True,index_label='Fila',
                 float_format='%.3e')

# Calculo del error con ISI y CCI,  pulso truncado
#-------------------------------------------------
snrv = [15,]
L = [2,6]
sirv = [15,]
delta5_esp = 1e-12
delta_tot = delta1_esp + delta5_esp
pe_isi_cci = []

for nulos in nulosv:
    tmn = np.linspace(-nulos*Ts, -Ts, nulos)
    tmx = np.linspace(Ts, nulos*Ts, nulos)
    t = np.concatenate((tmn,tmx))
    #selccion de bits que inciden en g0 despues del truncamiento
    bpsk_bits_t = bpsk_bits[int(nsymbols/2)-nulos-1:nulos+int(nsymbols/2)-1]
    for l in L:
        for sir in sirv:
            for snr in snrv:
                s = 10**(snr/20)
                for alpha in alphav:
                    for jttr in jitterv:
                        pe_isi_cci.append({'nulos':nulos,'SNR':snr,
                                           'rolloff':alpha,
                                           'SIR':sir,'L': l,
                                           'jitter':jttr})
                        for k in pulsos.keys():
                            #valor medio del pulso en la mitad del mensaje
                            g0 = s*pulsos[k](np.array([jttr]),Ts,alpha)[0]
                            Rk = g0/np.sqrt(l * 10**(sir/10.0)) * np.ones(l)
                            #valor medio de los demas pulsos en el mensaje
                            gk = s*bpsk_bits_t*pulsos[k](t + jttr,Ts,alpha)
                            #magnitud de la interferencia co-canal
                            Te = pe_T_d5_est(g0, gk, Rk, delta5_esp)
                            M = pe_M_est(delta1_esp, Te, 10)
                            #estimacion de Prob. error debido a CCI+ISI
                            pe, err_est = pe_isi_cci_awgn(g0,gk,Rk,M,Te)
                            if (err_est > delta_tot):
                                raise Exception("error est. Pe ISI+CCI>cota")
                            pe_isi_cci[-1][k] = pe #guardar error estimado
#creacion de archivo csv con los errores debido a CCI
pe_isiccifile = 'pe_isi_cci_truc.csv'
pe_isiccifile_path = os.path.join(dirplot, pe_isiccifile)
pe_isicci_df_truc = pd.DataFrame(pe_isi_cci)
print("\nError debido a ISI y CCI pulso truncado")
print("========================================")
print(pe_isicci_df)
pe_isicci_df_truc.to_csv(pe_isiccifile_path,index=False,float_format='%.3e')
