# Proyecto 1 de Comunicaciones Digitales Avanzadas (EL7041)

Descargar el repositorio mediante el siguiente comando:
`git clone  https://gitlab.com/rodrigo.munoz2/proyecto_1`

Luego, ingresar al directorio del proyecto (LINUX/WINDOWS):
`cd proyecto_1`

Los comandos siguientes asumen que se esta dentro del directorio `proyecto_1`.
## Ejecución en Linux (Distros basadas en Ubuntu > 18.04)

Este script necesita de las siguientes librerías de `python`:

- `numpy`
- `matplotlib`
- `scipy`
- `pandas`

Para instalar estas librerías, puede ejecutar lo siguiente:

`pip install -r requirements.txt`

Además, es necesario que el sistema tenga `LaTex` instalado para generar correctamente los textos en las gráficas. Para ello se deben instalar algunas librerías básicas mediante el siguiente comando:

`sudo apt-get install -y cm-super dvipng texlive-latex-extra texlive-fonts-recommended`
Ejecutar la simulación mediante:

`python main.py`

# Ejecución en Docker
Para crear la imagen de Docker se debe ejecutar el siguiente comando:

`docker build -t proyecto01_rml .`

Luego para ejecutar el script, el comando depende del sistema operativo:

- en Windows: `docker run -v %cd%:/proyecto1 proyecto01_rml`
- en Linux: `docker run -v $(pwd):/proyecto1 proyecto01_rml`

Luego de unos segundos, los resultados se encontrarán dentro una nueva carpeta llamada `img`.
